package com.example.tugas4;

import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;
import android.widget.Button;

public class MainActivity2 extends AppCompatActivity implements View.OnClickListener {
    TextView tv1, tv2, tv3, tv4;
    Button bt5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        bt5 = findViewById(R.id.button5);
        bt5.setOnClickListener(this);

        tv1 = findViewById(R.id.textView);
        tv3 = findViewById(R.id.textView3);
        tv2 = findViewById(R.id.textView2);
        tv4 = findViewById(R.id.textView4);

        Intent in = getIntent();
        Bundle b = in.getExtras();
        String input1 = b.getString("input1");
        String input2 = b.getString("input2");
        String op = b.getString("op");

        tv1.setText(input1);
        tv3.setText(input2);
        tv2.setText(op);

        double in1 = Double.parseDouble(input1);
        double in2 = Double.parseDouble(input2);
        double result = 0.0;

        switch(op){
            case "+": result = in1 + in2; break;
            case "-": result = in1 - in2; break;
            case "*": result = in1 * in2; break;
            case "÷": result = in1 / in2; break;
        }

        tv4.setText("= " + result);
    }

    @Override
    public void onClick(View view) {
        Intent i=new Intent(this, MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }
}