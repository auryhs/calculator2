package com.example.tugas4;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.content.Intent;
import android.widget.Button;
import android.widget.EditText;

import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    Button bt1,bt2,bt3,bt4;
    EditText et1, et2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bt1 = findViewById(R.id.button);
        bt1.setOnClickListener(this);
        bt2 = findViewById(R.id.button2);
        bt2.setOnClickListener(this);
        bt3 = findViewById(R.id.button3);
        bt3.setOnClickListener(this);
        bt4 = findViewById(R.id.button4);
        bt4.setOnClickListener(this);

        et1 = findViewById(R.id.editTextNumber);
        et2 = findViewById(R.id.editTextNumber2);
    }

    @Override
    public void onClick(View view) {
        Intent in = new Intent(this, MainActivity2.class);
        Bundle b = new Bundle();
        b.putString("input1", et1.getText().toString());
        b.putString("input2", et2.getText().toString());

        switch(view.getId()){
            case R.id.button : b.putString("op", "+"); break;
            case R.id.button2: b.putString("op", "-"); break;
            case R.id.button3: b.putString("op", "*"); break;
            case R.id.button4: b.putString("op", "÷"); break;
        }
        in.putExtras(b);
        startActivity(in);
    }
}